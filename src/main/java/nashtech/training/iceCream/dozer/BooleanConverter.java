package nashtech.training.iceCream.dozer;


import nashtech.training.iceCream.util.CommonUtil;
import org.dozer.DozerConverter;

/**
 * This coverts value between string and boolean.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public class BooleanConverter extends DozerConverter<String, Boolean>
{
    public BooleanConverter()
    {
        super(String.class, Boolean.class);
    }

    public BooleanConverter(Class<String> prototypeA, Class<Boolean> prototypeB)
    {
        super(prototypeA, prototypeB);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean convertTo(String source, Boolean destination)
    {
        if (CommonUtil.TRUE_BOOLEAN.equalsIgnoreCase(source)) {
            return Boolean.TRUE;
        }
        else if (CommonUtil.FALSE_BOOLEAN.equalsIgnoreCase(source)) {
            return Boolean.FALSE;
        }
        else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String convertFrom(Boolean source, String destination)
    {
        return Boolean.TRUE.equals(source) ? CommonUtil.TRUE_BOOLEAN : CommonUtil.FALSE_BOOLEAN;
    }
}
