package nashtech.training.iceCream.dozer;

import org.dozer.DozerConverter;

/**
 * This coverts value between string and integer.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public class IntegerConverter extends DozerConverter<String, Integer>
{
    public IntegerConverter()
    {
        super(String.class, Integer.class);
    }

    public IntegerConverter(Class<String> prototypeA, Class<Integer> prototypeB)
    {
        super(prototypeA, prototypeB);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer convertTo(String source, Integer destination)
    {
        return Integer.parseInt(source);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String convertFrom(Integer source, String destination)
    {
        return String.valueOf(source);
    }
}
