package nashtech.training.iceCream.dozer;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.dozer.DozerConverter;

import java.text.ParseException;
import java.util.Date;

/**
 * This coverts value between string and boolean.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public class DateConverter extends DozerConverter<String, Date>
{
    public DateConverter()
    {
        super(String.class, Date.class);
    }

    public DateConverter(Class<String> prototypeA, Class<Date> prototypeB)
    {
        super(prototypeA, prototypeB);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date convertTo(String source, Date destination)
    {
        try {
            return DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.parse(source);
        }
        catch (ParseException e) {
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String convertFrom(Date source, String destination)
    {
        return DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.format(source);
    }
}
