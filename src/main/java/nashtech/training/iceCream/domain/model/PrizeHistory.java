package nashtech.training.iceCream.domain.model;


import nashtech.training.iceCream.util.ApiUtil;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name="prize_history")
public class PrizeHistory  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_recipe_id")
    private UserRecipe userRecipe;

    @NotNull(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "enable status may be null or empty.")
    @Column(name = "enable_status")
    private Boolean enablestatus;

    public UserRecipe getUserRecipe() {
        return userRecipe;
    }

    public void setUserRecipe(UserRecipe userRecipe) {
        this.userRecipe = userRecipe;
    }

    public Boolean getEnablestatus() {
        return enablestatus;
    }

    public void setEnablestatus(Boolean enablestatus) {
        this.enablestatus = enablestatus;
    }
}
