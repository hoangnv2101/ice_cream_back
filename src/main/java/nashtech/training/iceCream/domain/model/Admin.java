package nashtech.training.iceCream.domain.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="admin")
public class Admin  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "username", unique = true)
    private String uasrname;

    @Column(name = "password")
    private String password;

    public String getUasrname() {
        return uasrname;
    }

    public void setUasrname(String uasrname) {
        this.uasrname = uasrname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

