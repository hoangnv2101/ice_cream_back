package nashtech.training.iceCream.domain.model;

import nashtech.training.iceCream.util.ApiUtil;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="recipe")
@NamedQueries({
        @NamedQuery(name = Recipe.ORDER_BY_VIEW_NUMBER_DESC,
                query = "from Recipe rc ORDER BY rc.viewNumber desc"),
        @NamedQuery(name = Recipe.GET_LIST_NEWEST,
                query = "from Recipe rc ORDER BY id desc"),
})
public class Recipe  implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String ORDER_BY_VIEW_NUMBER_DESC = "Recipe.getRecipeOrderByViewNumberDESC";
    public static final String GET_LIST_NEWEST = "Recipe.getListNewEst";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recipe_id")
    private Integer id;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "name may be null or empty.")
    @Size(max = 100)
    @Column(name = "name")
    private String name;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "image may be null or empty.")
    @Size(max = 100)
    @Column(name = "image")
    private String image;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "description may be null or empty.")
    @Column(name = "desciption")
    private String description;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "details may be null or empty.")
    @Column(name = "details")
    private String details;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "author may be null or empty.")
    @Size(max = 50)
    @Column(name = "author")
    private String author;

    @Column(name = "view_number")
    private int viewNumber;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "upload_date may be null or empty.")
    @Column(name = "upload_date")
    private Date uploadDate;

    @NotNull(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "enable status may be null or empty.")
    @Column(name = "enable_status")
    private Boolean enablestatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getViewNumber() {
        return viewNumber;
    }

    public void setViewNumber(int viewNumber) {
        this.viewNumber = viewNumber;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Boolean getEnablestatus() {
        return enablestatus;
    }

    public void setEnablestatus(Boolean enablestatus) {
        this.enablestatus = enablestatus;
    }
}
