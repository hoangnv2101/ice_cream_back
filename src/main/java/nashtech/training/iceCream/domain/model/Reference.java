package nashtech.training.iceCream.domain.model;

import nashtech.training.iceCream.util.ApiUtil;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name="reference")
public class Reference  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reference_id")
    private Integer id;

    @NotNull(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "monthly_fee may be null or empty.")
    @Column(name = "monthly_fee")
    private Double monthlyFee;

    @NotNull(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "yearly_fee may be null or empty.")
    @Column(name = "yearly_fee")
    private Double yearlyFee;

    @NotNull(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "book_cost may be null or empty.")
    @Column(name = "book_cost")
    private Double bookCost;

    public Integer getId() {
        return id;
    }

    public Double getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(Double monthlyFee) {
        this.monthlyFee = monthlyFee;
    }

    public Double getYearlyFee() {
        return yearlyFee;
    }

    public void setYearlyFee(Double yearlyFee) {
        this.yearlyFee = yearlyFee;
    }

    public Double getBookCost() {
        return bookCost;
    }

    public void setBookCost(Double bookCost) {
        this.bookCost = bookCost;
    }
}
