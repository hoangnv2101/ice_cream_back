package nashtech.training.iceCream.domain.model;

import nashtech.training.iceCream.util.ApiUtil;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="online_order")
public class OnlineOrder  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Integer id;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "name may be null or empty.")
    @Size(max = 50)
    @Column(name = "name")
    private String name;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "email may be null or empty.")
    @Size(max = 50)
    @Column(name = "email")
    private String email;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "contact may be null or empty.")
    @Size(max = 2048)
    @Column(name = "contact")
    private String contact;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "address may be null or empty.")
    @Size(max = 2048)
    @Column(name = "address")
    private String address;

    @NotNull(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "bookCost may be null or empty.")
    @Column(name = "book_cost")
    private Double bookCost;

    @NotBlank(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "payingOption may be null or empty.")
    @Size(max = 100)
    @Column(name = "paying_option")
    private String payingOption;

    @NotNull(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "orderDate may be null or empty.")
    @Column(name = "order_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderDate;

    @NotNull(message = ApiUtil.BLANK_REC + ApiUtil.ERROR_CODE_SPLIT + "status may be null or empty.")
    @Column(name = "status")
    private Boolean status;

    public Integer getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getBookCost() {
        return bookCost;
    }

    public void setBookCost(Double bookCost) {
        this.bookCost = bookCost;
    }

    public String getPayingOption() {
        return payingOption;
    }

    public void setPayingOption(String payingOption) {
        this.payingOption = payingOption;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
