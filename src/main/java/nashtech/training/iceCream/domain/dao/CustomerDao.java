package nashtech.training.iceCream.domain.dao;

import nashtech.training.iceCream.domain.model.Customer;

public interface CustomerDao extends GenericDao<Customer, Integer>{
}
