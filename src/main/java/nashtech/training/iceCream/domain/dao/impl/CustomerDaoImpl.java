package nashtech.training.iceCream.domain.dao.impl;

import nashtech.training.iceCream.domain.dao.CustomerDao;
import nashtech.training.iceCream.domain.model.Customer;
import nashtech.training.iceCream.helper.ObjectMapperHelper;
import org.hibernate.SessionFactory;

public class CustomerDaoImpl extends GenericDaoImpl<Customer, Integer> implements CustomerDao {

    /**
     * Constructor.
     *
     * @param sessionFactory     the session factory.
     * @param objectMapperHelper the object mapper helper.
     */
    public CustomerDaoImpl(SessionFactory sessionFactory, ObjectMapperHelper objectMapperHelper) {
        super(sessionFactory, objectMapperHelper);
    }
}
