package nashtech.training.iceCream.domain.dao;

import nashtech.training.iceCream.dto.Page;
import org.hibernate.LockMode;
import org.hibernate.Session;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * This defines common functions of generic data access object (DAO) interface.
 *
 * @param <T>  the generic entity object.
 * @param <ID> the generic identifier of the entity object.
 * @author nvanhoang
 * @since 16/06/2018
 */
public interface GenericDao<T extends Serializable, ID extends Serializable>
{
    /**
     * Find all entities by the property name and value.
     *
     * @param propertyName  the property name.
     * @param propertyValue the property value.
     * @return the list of entities.
     */
    List<T> findAllByProperty(String propertyName, Object propertyValue);

    /**
     * Find all entities by the property name and value and lock mode.
     *
     * @param propertyName  the property name.
     * @param propertyValue the property value.
     * @param mode          the lock mode.
     * @return the list of entities.
     */
    List<T> findAllByProperty(String propertyName, Object propertyValue, LockMode mode);

    /**
     * Find all entities by a map of property name and value.
     *
     * @param conditions the map of property name and value.
     * @return the list of entities.
     */
    List<T> findAllByProperties(Map<String, Object> conditions);

    /**
     * Find entities by a list of ids.
     *
     * @param values a list of the property values.
     * @return the list of entities.
     */
    List<T> findAllByProperty(String propertyName, List<Object> values);

    /**
     * Find one entity by property name and value.
     *
     * @param propertyName  the property name.
     * @param propertyValue the property value.
     * @return the entity.
     */
    T findOneByProperty(String propertyName, Object propertyValue);

    /**
     * Find one entity by the property name and value and lock mode.
     *
     * @param propertyName  the property name.
     * @param propertyValue the property value.
     * @param mode          the lock mode.
     * @return the entity.
     */
    T findOneByProperty(String propertyName, Object propertyValue, LockMode mode);

    /**
     * Find one entity by a map of property name and value.
     *
     * @param conditions the map of property name and value.
     * @return the entity.
     */
    T findOneByProperties(Map<String, Object> conditions);



    /**
     * Find all entities.
     *
     * @return the list of entities.
     */
    List<T> findAll();

    /**
     * Find entity by id.
     *
     * @param id the id.
     * @return the entity.
     */
    T findById(ID id);

    /**
     * Refresh the object.
     *
     * @param value the object.
     * @param mode  the lock mode.
     */
    void refresh(Object value, LockMode mode);

    /**
     * Count all entities.
     *
     * @return the number of all entities.
     */
    long countAll();

    /**
     * Save or Update the entity.
     *
     * @param entity the entity.
     * @return the saved or updated entity.
     */
    T saveOrUpdate(T entity);

    /**
     * Save the entity.
     *
     * @param entity the entity.
     * @return the saved entity.
     */
    T save(T entity);

    /**
     * Save or Update a collection of entities.
     *
     * @param entities the collection of entities.
     */
    void saveOrUpdate(Collection<T> entities);

    /**
     * Save a collection of entities.
     *
     * @param entities the collection of entities.
     */
    void save(Collection<T> entities);

    /**
     * Merge the entity.
     *
     * @param detachedEntity the entity.
     * @return the entity.
     */
    T merge(T detachedEntity);

    /**
     * Merge a collection of entities.
     *
     * @param entities the collection of entities.
     */
    void merge(Collection<T> entities);

    /**
     * Delete the entity.
     *
     * @param entity the entity.
     */
    void delete(T entity);

    /**
     * Delete a collection of entities.
     *
     * @param entities the collection of entities.
     */
    void delete(Collection<T> entities);

    /**
     * Delete entity by id.
     *
     * @param id the id.
     */
    void deleteById(ID id);

    /**
     * Delete entities by collection of ids.
     *
     * @param ids the collection of ids.
     */
    void deleteById(Collection<ID> ids);

    /**
     * Delete all entities.
     *
     * @return the number of deleted entities.
     */
    long deleteAll();

    /**
     * Delete all entities and association.
     */
    void deleteAllCascade();

    /**
     * Evict the entity.
     *
     * @param entity the entity.
     */
    void evict(T entity);

    /**
     * Flush the session.
     */
    void flush();

    /**
     * Clear the session.
     */
    void clear();

    /**
     * Execute the SQL command.
     *
     * @param sqlCommnad the SQL command.
     * @return the list of entities.
     */
    List executeQuery(String sqlCommnad);

    /**
     * Search entities based on  search conditions, sort and pagination.
     *
     * @param paras      the parameter map of property name and value.
     * @param searchText the text search (like search) based on pre-defined properties.
     * @param sort       the sorted properties.
     * @param page       the page index.
     * @param perPage    the page size.
     * @return the list of entities.
     */
    Page<T> search(Map<String, String> paras, String searchText, String sort, Integer page,
                   Integer perPage);

    /**
     * Get the session.
     *
     * @return the session.
     */
    Session getSession();

}