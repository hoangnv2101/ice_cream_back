package nashtech.training.iceCream.domain.dao.impl;

import nashtech.training.iceCream.domain.dao.RecipeDao;
import nashtech.training.iceCream.domain.model.Recipe;
import nashtech.training.iceCream.helper.ObjectMapperHelper;
import org.hibernate.SessionFactory;

import java.util.List;

public class RecipeDaoImpl extends GenericDaoImpl<Recipe, Integer> implements RecipeDao {
    /**
     * Constructor.
     *
     * @param sessionFactory     the session factory.
     * @param objectMapperHelper the object mapper helper.
     */
    public RecipeDaoImpl(SessionFactory sessionFactory, ObjectMapperHelper objectMapperHelper) {
        super(sessionFactory, objectMapperHelper);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public List<Recipe> getRecipeOrderByViewNumberDesc(int maxResult) {
        return (List<Recipe>) getSession().getNamedQuery(Recipe.ORDER_BY_VIEW_NUMBER_DESC)
                .setMaxResults(maxResult).list();
    }

    @Override
    public List<Recipe> getRecipesNewest(int maxResult) {
        return (List<Recipe>) getSession().getNamedQuery(Recipe.GET_LIST_NEWEST)
                .setMaxResults(maxResult).list();
    }
}
