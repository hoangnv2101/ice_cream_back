package nashtech.training.iceCream.domain.dao;

import nashtech.training.iceCream.domain.model.OnlineOrder;

public interface OnlineOrderDao  extends GenericDao<OnlineOrder, Integer>{

}
