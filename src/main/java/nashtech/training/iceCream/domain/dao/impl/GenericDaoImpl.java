package nashtech.training.iceCream.domain.dao.impl;


import nashtech.training.iceCream.domain.dao.GenericDao;
import nashtech.training.iceCream.dto.Page;
import nashtech.training.iceCream.dto.api.HttpStatus;
import nashtech.training.iceCream.helper.ObjectMapperHelper;
import nashtech.training.iceCream.util.ApiUtil;
import nashtech.training.iceCream.util.CommonUtil;
import nashtech.training.iceCream.util.ReflectionUtil;
import org.hibernate.*;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Id;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

/**
 * This is generic implementation of {@link GenericDao}.
 *
 * @param <T>  the generic entity object.
 * @param <ID> the generic identifier of the entity object.
 * @author nvanhoang
 * @since 21/06/2018
 */
@Transactional
public abstract class GenericDaoImpl<T extends Serializable, ID extends Serializable>
    implements GenericDao<T, ID>
{

    private final Class<T> persistentClass;

    private final SessionFactory sessionFactory;
    private final ObjectMapperHelper objectMapperHelper;
    private List<String> searchTextProperties;
    private List<String> defaultOrderProperties;
    private Integer defaultPage = ApiUtil.DEFAULT_PAGE;
    private Integer defaultPerPage = ApiUtil.DEFAULT_PERPAGE;

    /**
     * Constructor.
     *
     * @param sessionFactory     the session factory.
     * @param objectMapperHelper the object mapper helper.
     */
    public GenericDaoImpl(SessionFactory sessionFactory, ObjectMapperHelper objectMapperHelper)
    {
        this.sessionFactory = sessionFactory;
        this.objectMapperHelper = objectMapperHelper;
        Class genericClass = getClass();
        while (genericClass != null && !(genericClass
            .getGenericSuperclass() instanceof ParameterizedType)) {
            genericClass = genericClass.getSuperclass();
        }
        persistentClass = (Class<T>) ((ParameterizedType) genericClass.getGenericSuperclass())
            .getActualTypeArguments()[0];
    }

    /**
     * Setter.
     *
     * @param searchTextProperties the list of properties for text search.
     */
    public void setSearchTextProperties(String searchTextProperties)
    {
        this.searchTextProperties = new ArrayList<String>(
            Arrays.asList(searchTextProperties.split(CommonUtil.COMMA_SPLIT)));
    }

    /**
     * Setter.
     *
     * @param defaultOrderProperties the list of properties for text search.
     */
    public void setDefaultOrderProperties(String defaultOrderProperties)
    {
        this.defaultOrderProperties = new ArrayList<String>(
            Arrays.asList(defaultOrderProperties.split(CommonUtil.COMMA_SPLIT)));
    }

    /**
     * Setter.
     *
     * @param defaultPage the default page index.
     */
    public void setDefaultPage(Integer defaultPage)
    {
        this.defaultPage = defaultPage;
    }

    /**
     * Setter.
     *
     * @param defaultPerPage the default page size.
     */
    public void setDefaultPerPage(Integer defaultPerPage)
    {
        this.defaultPerPage = defaultPerPage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> findAllByProperty(String propertyName, Object propertyValue)
    {
        return (List<T>) createCriteria().add(Restrictions.eq(propertyName, propertyValue)).list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> findAllByProperty(String propertyName, Object propertyValue,
                                     LockMode mode)
    {
        return (List<T>) createCriteria().add(Restrictions.eq(propertyName, propertyValue))
            .setLockMode(mode).list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> findAllByProperties(Map<String, Object> conditions)
    {
        Criteria criteria = createCriteria();
        for (String attribute : conditions.keySet()) {
            criteria.add(Restrictions.eq(attribute, conditions.get(attribute)));
        }
        return criteria.list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T findOneByProperty(String propertyName, Object propertyValue)
    {
        return (T) createCriteria().add(Restrictions.eq(propertyName, propertyValue))
            .uniqueResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T findOneByProperties(Map<String, Object> conditions)
    {
        Criteria criteria = createCriteria();
        for (String attribute : conditions.keySet()) {
            criteria.add(Restrictions.eq(attribute, conditions.get(attribute)));
        }
        List<T> rs = criteria.list();
        if (rs.size() > 0) {
            return rs.get(0);
        }
        else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T findOneByProperty(String propertyName, Object propertyValue, LockMode mode)
    {
        return (T) createCriteria().add(Restrictions.eq(propertyName, propertyValue))
            .setLockMode(mode).uniqueResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> findAll()
    {
        return list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> findAllByProperty(String propertyName, List<Object> values)
    {
        Query query = getSession()
            .createQuery(
                "from " + persistentClass.getName() + " where " + propertyName + " in :values");
        query.setParameterList("values", values);

        return query.list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T findById(ID id)
    {
        return getSession().get(getPersistentClass(), id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh(Object value, LockMode mode)
    {
        getSession().refresh(value, mode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long countAll()
    {
        Criteria crit = getSession().createCriteria(getPersistentClass())
            .setProjection(Projections.rowCount());
        Object obj = crit.uniqueResult();
        if (obj instanceof Integer) {
            return ((Integer) obj).longValue();
        }
        return (Long) crit.uniqueResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T saveOrUpdate(T entity)
    {
        getSession().saveOrUpdate(entity);
        return entity;
    }

    /**
     * {@inheritDoc}
     */
    @Override public T save(T entity)
    {
        getSession().save(entity);
        return entity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveOrUpdate(Collection<T> entities)
    {
        Session session = getSession();
        for (T entity : entities) {
            session.saveOrUpdate(entity);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override public void save(Collection<T> entities)
    {
        Session session = getSession();
        for (T entity : entities) {
            session.save(entity);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T merge(T detachedEntity)
    {
        return (T) getSession().merge(detachedEntity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void merge(Collection<T> entities)
    {
        Session session = getSession();
        for (T entity : entities) {
            session.merge(entity);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(T entity)
    {
        getSession().delete(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Collection<T> entities)
    {
        Session session = getSession();
        for (T entity : entities) {
            session.delete(entity);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteById(ID id)
    {
        List<ID> ids = new ArrayList<ID>();
        ids.add(id);
        deleteById(ids);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteById(Collection<ID> ids)
    {
        Session session = getSession();
        Query query = session
            .createQuery("delete " + persistentClass.getName() + " where id in :ids");

        query.setParameterList("ids", ids);
        query.executeUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long deleteAll()
    {
        return getSession().createQuery("delete from " + getPersistentClass().getName())
            .executeUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAllCascade()
    {
        List<T> all = findAll();
        delete(all);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void evict(T entity)
    {
        getSession().evict(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void flush()
    {
        getSession().flush();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear()
    {
        getSession().clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List executeQuery(String sqlCommnad)
    {
        return getSession().createQuery(sqlCommnad).list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<T> search(Map<String, String> paras, String searchText, String sort, Integer page,
                          Integer perPage)
    {
        List<T> entities = new ArrayList<T>();

        Criteria criteria = createCriteria();

        Map<String, Criteria> associations = new HashMap<String, Criteria>();

        if (paras != null && !paras.isEmpty()) {
            criteria = processSearchParas(criteria, paras, associations);
        }

        criteria = processSearhText(criteria, searchText, associations);

        Field field = ReflectionUtil.findField(getPersistentClass(), Id.class);
        String uniqueField = field.getName();

        // Get total of rows based on search conditions.
        Projection idCountProjection = Projections.countDistinct(uniqueField);
        criteria.setProjection(idCountProjection);

        long totalResultCount = 0;
        Object obj = criteria.uniqueResult();
        if (obj instanceof Integer) {
            totalResultCount = ((Integer) obj).longValue();
        }
        else {
            totalResultCount = (Long) criteria.uniqueResult();
        }

        if (totalResultCount == 0) {
            return new Page<T>(totalResultCount, entities);
        }

        // Get a list of ids based on search conditions and pagination in order.
        criteria.setProjection(Projections.distinct(Projections.property(uniqueField)));
        criteria = processSort(criteria, sort, associations);

        perPage = perPage != null ? perPage : defaultPerPage;
        if (perPage > 0) {
            criteria.setMaxResults(perPage);
        }

        page = page != null ? page : defaultPage;
        if (page >= 0 && perPage > 0) {
            criteria.setFirstResult(page * perPage);
        }

        List uniqueSubList = criteria.list();

        if (uniqueSubList == null || uniqueSubList.isEmpty()) {
            return new Page<T>(totalResultCount, entities);
        }

        // Get a list of entities based on search conditions and pagination in order.
        Criteria newCriteria = createCriteria();
        newCriteria.setFirstResult(0);
        newCriteria.setMaxResults(Integer.MAX_VALUE);

        Map<String, Criteria> newAssociations = new HashMap<String, Criteria>();
        newCriteria = processSort(newCriteria, sort, newAssociations);

        newCriteria.add(Restrictions.in(uniqueField, uniqueSubList));

        try {
            entities.addAll(newCriteria.list());
        }
        catch (Exception e) {
            throw CommonUtil
                .toErrorsResponse(getPersistentClass().getSimpleName(), paras.keySet().toString(),
                                  ApiUtil.INCORRECT_VALUE_REC,
                                  e.getMessage(),
                                  HttpStatus.UNPROCESSABLE_ENTITY.toString());
        }

        return new Page<T>(totalResultCount, entities);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Session getSession()
    {
        return sessionFactory.getCurrentSession();
    }


    /**
     * Process search parameters.
     *
     * @param criteria the criteria.
     * @param paras    the search parameters.
     * @return the criteria.
     */
    private Criteria processSearchParas(Criteria criteria, Map<String, String> paras,
                                        Map<String, Criteria> associations)
    {
        for (String propertyName : paras.keySet()) {
            String value = paras.get(propertyName);
            if (propertyName != null && !propertyName.isEmpty() && value != null && !value
                .isEmpty()) {

                String fieldName = propertyName;
                Class<?> kclass = getPersistentClass();

                String[] names = propertyName.split(CommonUtil.DOT_SPLIT);
                if (names.length == 2) {
                    if (associations.get(names[0]) == null) {
                        Criteria associatedCriteria = criteria.createAlias(names[0], names[0],
                                                                           JoinType.LEFT_OUTER_JOIN);
                        associations.put(names[0], associatedCriteria);
                    }
                    fieldName = names[1];

                    Field associatedField = ReflectionUtil.findField(kclass, names[0]);

                    Type genericType = associatedField.getGenericType();
                    if (genericType instanceof ParameterizedType) {
                        kclass = (Class<?>) ((ParameterizedType) genericType)
                            .getActualTypeArguments()[0];
                    }
                    else {
                        kclass = associatedField.getType();
                    }

                }

                Field field = ReflectionUtil.findField(kclass, fieldName);
                if (field != null) {
                    List<Criterion> criterions = process(field, propertyName, value);
                    for (Criterion criterion : criterions) {
                        criteria.add(criterion);
                    }
                }
                else {
                    throw CommonUtil
                        .toErrorsResponse(getPersistentClass().getSimpleName(), propertyName,
                                          ApiUtil.INVALID_TYPE_REC,
                                          ApiUtil.INVALID_TYPE_REC_MSG,
                                          HttpStatus.UNPROCESSABLE_ENTITY.toString());
                }
            }
        }

        return criteria;
    }

    /**
     * Process search value.
     *
     * @param field          the property field.
     * @param propertyValues the search value.
     * @return the list of criterion.
     */
    private List<Criterion> process(Field field, String propertyName, String propertyValues)
    {
        List<Criterion> criterions = new ArrayList<Criterion>();
        try {
            if (propertyValues.startsWith("[") && propertyValues.endsWith("]")) {
                String value = propertyValues.substring(1, propertyValues.length() - 1);
                String[] values = value.split(CommonUtil.COMMA_SPLIT);
                if (values.length == 2) {
                    criterions
                        .add(Restrictions.ge(propertyName, process(field, values[0])));
                    criterions
                        .add(Restrictions.le(propertyName, process(field, values[1])));
                }
                else {
                    throw CommonUtil
                        .toErrorsResponse(getPersistentClass().getSimpleName(), propertyName,
                                          ApiUtil.INCORRECT_VALUE_REC,
                                          ApiUtil.INVALID_VALUE_REC_MSG,
                                          HttpStatus.UNPROCESSABLE_ENTITY.toString());
                }
            }
            else if (propertyValues.startsWith("<") && propertyValues.endsWith(">")) {
                String value = propertyValues.substring(1, propertyValues.length() - 1);
                String[] values = value.split(CommonUtil.COMMA_SPLIT);
                if (values.length == 2) {
                    criterions
                        .add(Restrictions.gt(propertyName, process(field, values[0])));
                    criterions
                        .add(Restrictions.lt(propertyName, process(field, values[1])));
                }
                else {
                    throw CommonUtil
                        .toErrorsResponse(getPersistentClass().getSimpleName(), propertyName,
                                          ApiUtil.INCORRECT_VALUE_REC,
                                          ApiUtil.INVALID_VALUE_REC_MSG,
                                          HttpStatus.UNPROCESSABLE_ENTITY.toString());
                }
            }
            else if (propertyValues.startsWith(">=")) {
                String value = propertyValues.substring(2);
                criterions.add(Restrictions.ge(propertyName, process(field, value)));
            }
            else if (propertyValues.startsWith(">")) {
                String value = propertyValues.substring(1);
                criterions.add(Restrictions.gt(propertyName, process(field, value)));
            }
            else if (propertyValues.startsWith("<=")) {
                String value = propertyValues.substring(2);
                criterions.add(Restrictions.le(propertyName, process(field, value)));
            }
            else if (propertyValues.startsWith("<")) {
                String value = propertyValues.substring(1);
                criterions.add(Restrictions.lt(propertyName, process(field, value)));
            }
            else {
                String[] values = propertyValues.split(CommonUtil.COMMA_SPLIT);
                List<Object> objectValues = new ArrayList<Object>();
                for (String value : values) {
                    objectValues.add(process(field, value));
                }
                if (!objectValues.isEmpty()) {
                    criterions.add(Restrictions.in(propertyName, objectValues));
                }
            }
        }
        catch (Exception e) {
            throw CommonUtil
                .toErrorsResponse(getPersistentClass().getSimpleName(), propertyName,
                                  ApiUtil.INCORRECT_VALUE_REC, ApiUtil.INVALID_VALUE_REC_MSG,
                                  HttpStatus.UNPROCESSABLE_ENTITY.toString());
        }

        return criterions;
    }

    /**
     * Process string value to real value of field..
     *
     * @param field the field.
     * @param value the string value.
     * @return The real object value.
     * @throws UnsupportedEncodingException
     */
    private Object process(Field field, String value) throws UnsupportedEncodingException
    {
        Object objectValue = value.trim();
        try {
            objectValue = objectMapperHelper.map(objectValue, field.getType());
        }
        catch (Exception e) {
            throw CommonUtil
                .toErrorsResponse(getPersistentClass().getSimpleName(), field.getName(),
                                  ApiUtil.INCORRECT_VALUE_REC,
                                  ApiUtil.INVALID_VALUE_REC_MSG,
                                  HttpStatus.UNPROCESSABLE_ENTITY.toString());
        }
        return objectValue;
    }

    /**
     * Process search text.
     *
     * @param criteria   the criteria.
     * @param searchText the search text.
     * @return
     */
    private Criteria processSearhText(Criteria criteria, String searchText,
                                      Map<String, Criteria> associations)
    {
        if (searchTextProperties != null && !searchTextProperties.isEmpty() && searchText != null
            && !searchText.isEmpty()) {
            Disjunction or = Restrictions.disjunction();
            String propertyValue =
                CommonUtil.PROPERTY_LIKE + searchText.trim() + CommonUtil.PROPERTY_LIKE;
            for (String propertyName : searchTextProperties) {
                String[] relations = propertyName
                    .split(CommonUtil.DOT_SPLIT);
                if (relations.length == 2 && associations.get(relations[0]) == null) {
                    Criteria associatedCriteria = criteria.createAlias(relations[0], relations[0],
                                                                       JoinType.LEFT_OUTER_JOIN);
                    associations.put(relations[0], associatedCriteria);
                }
                or.add(Restrictions.like(propertyName, propertyValue));
            }
            criteria.add(or);
        }
        return criteria;
    }

    /**
     * Process sort properties.
     *
     * @param criteria the criteria.
     * @param sort     the sort properties.
     * @return the criteria.
     */
    private Criteria processSort(Criteria criteria, String sort, Map<String, Criteria> associations)
    {
        List<String> sortList = defaultOrderProperties;
        if (sort != null && !sort.isEmpty()) {
            String[] sortArray = sort.split(CommonUtil.COMMA_SPLIT);
            sortList = Arrays.asList(sortArray);
        }

        if (sortList != null && !sortList.isEmpty()) {
            sortList.stream().filter(i -> i != null).forEach(i -> {
                String[] orders = i.split(CommonUtil.COLON_SPLIT);

                String[] relations = orders[0].split(CommonUtil.DOT_SPLIT);
                if (relations.length == 2 && associations.get(relations[0]) == null) {
                    Criteria associatedCriteria = criteria.createAlias(relations[0], relations[0],
                                                                       JoinType.LEFT_OUTER_JOIN);
                    associations.put(relations[0], associatedCriteria);
                }

                if (orders.length == 1) {
                    criteria.addOrder(Order.asc(orders[0]));
                }
                else if (orders.length == 2) {
                    if ("asc".equalsIgnoreCase(orders[1])) {
                        criteria.addOrder(Order.asc(orders[0]));
                    }
                    else if ("desc".equalsIgnoreCase(orders[1])) {
                        criteria.addOrder(Order.desc(orders[0]));
                    }
                }
                else {
                    throw CommonUtil
                        .toErrorsResponse(getPersistentClass().getSimpleName(), ApiUtil.SORT_BY,
                                          ApiUtil.INCORRECT_VALUE_REC,
                                          ApiUtil.INVALID_VALUE_REC_MSG,
                                          HttpStatus.UNPROCESSABLE_ENTITY.toString());
                }
            });
        }

        return criteria;
    }

    /**
     * List entities base on the array of criteria.
     *
     * @param criteria the array of criteria.
     * @return the list of entities.
     */
    private List<T> list(Criterion... criteria)
    {
        Criteria crit = createCriteria();
        for (Criterion c : criteria) {
            crit.add(c);
        }
        return crit.list();
    }

    /**
     * Create criteria.
     *
     * @return the criteria.
     */
    private Criteria createCriteria()
    {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria;
    }

    /**
     * Get the persistent class.
     *
     * @return the persistent class.
     */
    private Class<T> getPersistentClass()
    {
        return persistentClass;
    }
}