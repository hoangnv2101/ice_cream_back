package nashtech.training.iceCream.domain.dao;

import nashtech.training.iceCream.domain.model.Recipe;

import java.util.List;

public interface RecipeDao  extends GenericDao<Recipe, Integer>{


    /**
     * Find entities order by view number.
     *
     * @param maxResult  max result to return.
     * @return the list of entities.
     */
    List<Recipe> getRecipeOrderByViewNumberDesc(int maxResult);

    /**
     * Find entities order by id.
     *
     * @param maxResult  max result to return.
     * @return the list of entities.
     */
    List<Recipe> getRecipesNewest(int maxResult);
}
