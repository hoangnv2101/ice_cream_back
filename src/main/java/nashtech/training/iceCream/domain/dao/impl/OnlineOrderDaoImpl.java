package nashtech.training.iceCream.domain.dao.impl;

import nashtech.training.iceCream.domain.dao.OnlineOrderDao;
import nashtech.training.iceCream.domain.model.OnlineOrder;
import nashtech.training.iceCream.helper.ObjectMapperHelper;
import org.hibernate.SessionFactory;

public class OnlineOrderDaoImpl extends GenericDaoImpl<OnlineOrder, Integer> implements OnlineOrderDao {
    /**
     * Constructor.
     *
     * @param sessionFactory     the session factory.
     * @param objectMapperHelper the object mapper helper.
     */
    public OnlineOrderDaoImpl(SessionFactory sessionFactory, ObjectMapperHelper objectMapperHelper) {
        super(sessionFactory, objectMapperHelper);
    }
}