package nashtech.training.iceCream.util;


import nashtech.training.iceCream.dto.api.Errors;
import nashtech.training.iceCream.dto.api.ErrorsResponse;
import nashtech.training.iceCream.dto.api.Meta;

import java.util.regex.Pattern;

/**
 * This is common util that defines constant and util functions.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public abstract class CommonUtil
{


    public static final String UTF_8 = "UTF-8";

    public static final String COMMA_SPLIT = ",";
    public static final String EQUAL_SPLIT = Pattern.quote("=");
    public static final String AND_SPLIT = Pattern.quote("&");
    public static final String TRUE_BOOLEAN = "true";
    public static final String FALSE_BOOLEAN = "false";

    public static final String VISITING_TYPE = "visiting type";
    public static final String DOT_SPLIT = Pattern.quote(".");
    public static final String PROPERTY_LIKE = "%";
    public static final String COLON_SPLIT = ":";



    /* Util Functions */

    /**
     * Convert to an ErrorsResponse
     *
     * @param resource   Class name
     * @param field      Field name
     * @param code       code
     * @param message    message error
     * @param httpStatus HTTP status
     * @return an ErrorsResponse
     */
    public static ErrorsResponse toErrorsResponse(String resource, String field, String code,
                                                  String message,
                                                  String httpStatus)
    {
        ErrorsResponse errorsResponse = new ErrorsResponse.Builder()
            .errors(CommonUtil.toErrors(resource, field, code, message, httpStatus)).build();
        return errorsResponse;
    }

    /**
     * Convert to an Errors
     *
     * @param resource   Class name
     * @param field      Field name
     * @param code       code
     * @param message    message error
     * @param httpStatus HTTP status
     * @return an Errors
     */
    public static Errors toErrors(String resource, String field, String code, String message,
                                  String httpStatus)
    {
        nashtech.training.iceCream.dto.api.Error error = new nashtech.training.iceCream.dto.api.Error(resource, field, code, message, null);
        Errors errors = new Errors(error, new Meta.Builder().httpStatus(httpStatus).build());
        return errors;
    }
}
