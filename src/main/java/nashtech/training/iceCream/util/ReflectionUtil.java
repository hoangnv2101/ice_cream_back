package nashtech.training.iceCream.util;

import org.springframework.util.Assert;
import org.springframework.util.ConcurrentReferenceHashMap;
import org.springframework.util.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * This is reflection util.
 *
 * @author nvanhoang
 * @since 21/06/2018
 */
public abstract class ReflectionUtil
{

    private static final Method[] NO_METHODS = new Method[0];
    private static final Field[] NO_FIELDS = new Field[0];
    private static final Map<Class<?>, Method[]> declaredMethodsCache = new ConcurrentReferenceHashMap(
        256);
    private static final Map<Class<?>, Field[]> declaredFieldsCache = new ConcurrentReferenceHashMap(
        256);

    /**
     * Find field
     *
     * @param clazz      class
     * @param annotation annotation
     * @return a field
     */
    public static Field findField(Class<?> clazz, Class<? extends Annotation> annotation)
    {
        List<Field> fields = findFields(clazz, annotation, (Class) null, true);
        return (fields != null && !fields.isEmpty()) ? fields.get(0) : null;
    }

    /**
     * Find fields
     *
     * @param clazz      class
     * @param annotation annotation
     * @return a list of field
     */
    public static List<Field> findFields(Class<?> clazz, Class<? extends Annotation> annotation)
    {
        return findFields(clazz, annotation, (Class) null, false);
    }

    /**
     * Find fields
     *
     * @param clazz      class
     * @param annotation annotation
     * @param type       type
     * @param onlyFirst  is only first?
     * @return a list of field
     */
    public static List<Field> findFields(Class<?> clazz, Class<? extends Annotation> annotation,
                                         Class<?> type, boolean onlyFirst)
    {
        Assert.notNull(clazz, "Class must not be null");
        Assert.isTrue(annotation != null || type != null,
                      "Either annotation or type of the field must be specified");

        Set<Field> fieldSet = new HashSet<Field>();
        for (Class searchType = clazz;
             Object.class != searchType && searchType != null; searchType = searchType
            .getSuperclass()) {
            Field[] fields = getDeclaredFields(searchType);
            Field[] var5 = fields;
            int var6 = fields.length;

            for (int var7 = 0; var7 < var6; ++var7) {
                Field field = var5[var7];
                if ((annotation == null || field.isAnnotationPresent(annotation) || field.getType()
                    .isAnnotationPresent(annotation)) && (type == null || type
                    .equals(field.getType()))) {
                    fieldSet.add(field);
                    if (onlyFirst) {
                        return new ArrayList<Field>(fieldSet);
                    }
                }
            }
        }

        return new ArrayList<Field>(fieldSet);
    }

    /**
     * Get declare fields
     *
     * @param clazz class
     * @return array fields
     */
    private static Field[] getDeclaredFields(Class<?> clazz)
    {
        Field[] result = (Field[]) declaredFieldsCache.get(clazz);
        if (result == null) {
            result = clazz.getDeclaredFields();
            declaredFieldsCache.put(clazz, result.length == 0 ? NO_FIELDS : result);
        }

        return result;
    }

    /**
     * Find field
     *
     * @param clazz class
     * @param name  field name
     * @return a field
     */
    public static Field findField(Class<?> clazz, String name)
    {
        return findField(clazz, name, (Class) null);
    }

    /**
     * Find field
     *
     * @param clazz class
     * @param name  field name
     * @param type  type
     * @return a field
     */
    public static Field findField(Class<?> clazz, String name, Class<?> type)
    {
        Assert.notNull(clazz, "Class must not be null");
        Assert.isTrue(name != null || type != null,
                      "Either name or type of the field must be specified");

        for (Class searchType = clazz;
             Object.class != searchType && searchType != null; searchType = searchType
            .getSuperclass()) {
            Field[] fields = getDeclaredFields(searchType);
            Field[] var5 = fields;
            int var6 = fields.length;

            for (int var7 = 0; var7 < var6; ++var7) {
                Field field = var5[var7];
                if ((name == null || name.equals(field.getName())) && (type == null || type
                    .equals(field.getType()))) {
                    return field;
                }
            }
        }

        return null;
    }

    /**
     * Get field
     *
     * @param field  field
     * @param target target
     * @return an object
     */
    public static Object getField(Field field, Object target)
    {
        try {
            return field.get(target);
        }
        catch (IllegalAccessException var3) {
            throw new IllegalStateException(
                "Unexpected reflection exception - " + var3.getClass().getName() + ": " + var3
                    .getMessage());
        }
    }

    /**
     * set value for field
     *
     * @param field  field
     * @param target target
     * @param value  value to set
     */
    public static void setField(Field field, Object target, Object value)
    {
        try {
            field.set(target, value);
        }
        catch (IllegalAccessException var4) {
            throw new IllegalStateException(
                "Unexpected reflection exception - " + var4.getClass().getName() + ": " + var4
                    .getMessage());
        }
    }

    /**
     * Find getter method
     *
     * @param clazz class
     * @param field field
     * @return a Method object
     */
    public static Method findGetterMethod(Class<?> clazz, Field field)
    {
        return findMethod(clazz, "get" + StringUtils.capitalize(field.getName()), null);
    }

    /**
     * Find setter method
     *
     * @param clazz class
     * @param field field
     * @return a Method object
     */
    public static Method findSetterMethod(Class<?> clazz, Field field)
    {
        return findMethod(clazz, "set" + StringUtils.capitalize(field.getName()), null);
    }

    /**
     * Find method
     *
     * @param clazz class
     * @param name  method name
     * @return a Method object
     */
    public static Method findMethod(Class<?> clazz, String name)
    {
        return findMethod(clazz, name, new Class[0]);
    }

    /**
     * Find method
     *
     * @param clazz      class
     * @param name       method name
     * @param paramTypes parameter type
     * @return a Method object
     */
    public static Method findMethod(Class<?> clazz, String name, Class... paramTypes)
    {
        Assert.notNull(clazz, "Class must not be null");
        Assert.notNull(name, "Method name must not be null");

        for (Class searchType = clazz;
             searchType != null; searchType = searchType.getSuperclass()) {
            Method[] methods =
                searchType.isInterface() ? searchType.getMethods() : getDeclaredMethods(searchType);
            Method[] var5 = methods;
            int var6 = methods.length;

            for (int var7 = 0; var7 < var6; ++var7) {
                Method method = var5[var7];
                if (name.equals(method.getName()) && (paramTypes == null || Arrays
                    .equals(paramTypes, method.getParameterTypes()))) {
                    return method;
                }
            }
        }

        return null;
    }

    /**
     * Invoke a method
     *
     * @param method method
     * @param target target
     * @return an object
     */
    public static Object invokeMethod(Method method, Object target)
    {
        return invokeMethod(method, target, new Object[0]);
    }

    /**
     * Invoke method
     *
     * @param method method
     * @param target target
     * @param args   array argument
     * @return an object
     */
    public static Object invokeMethod(Method method, Object target, Object... args)
    {
        try {
            return method.invoke(target, args);
        }
        catch (Exception var4) {
            throw new IllegalStateException("Should never get here");
        }
    }

    /**
     * Get declare method
     *
     * @param clazz class
     * @return array Method
     */
    private static Method[] getDeclaredMethods(Class<?> clazz)
    {
        Method[] result = (Method[]) declaredMethodsCache.get(clazz);
        if (result == null) {
            Method[] declaredMethods = clazz.getDeclaredMethods();
            List defaultMethods = findConcreteMethodsOnInterfaces(clazz);
            if (defaultMethods != null) {
                result = new Method[declaredMethods.length + defaultMethods.size()];
                System.arraycopy(declaredMethods, 0, result, 0, declaredMethods.length);
                int index = declaredMethods.length;

                for (Iterator var5 = defaultMethods.iterator(); var5.hasNext(); ++index) {
                    Method defaultMethod = (Method) var5.next();
                    result[index] = defaultMethod;
                }
            }
            else {
                result = declaredMethods;
            }

            declaredMethodsCache.put(clazz, result.length == 0 ? NO_METHODS : result);
        }

        return result;
    }

    /**
     * Get a list of method
     *
     * @param clazz class
     * @return list of method
     */
    private static List<Method> findConcreteMethodsOnInterfaces(Class<?> clazz)
    {
        LinkedList result = null;
        Class[] var2 = clazz.getInterfaces();
        int var3 = var2.length;

        for (int var4 = 0; var4 < var3; ++var4) {
            Class ifc = var2[var4];
            Method[] var6 = ifc.getMethods();
            int var7 = var6.length;

            for (int var8 = 0; var8 < var7; ++var8) {
                Method ifcMethod = var6[var8];
                if (!Modifier.isAbstract(ifcMethod.getModifiers())) {
                    if (result == null) {
                        result = new LinkedList();
                    }

                    result.add(ifcMethod);
                }
            }
        }

        return result;
    }
}
