package nashtech.training.iceCream.util;
import nashtech.training.iceCream.dto.Page;
import nashtech.training.iceCream.dto.api.*;
import nashtech.training.iceCream.service.GenericService;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;

/**
 * This is common util that defines constant and util functions.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public abstract class ApiUtil
{
    /* Route Constants */
    public static final String ROOT_URL = "/rest";

    public static final String CUSTOMER_URL = ROOT_URL + "/customers";
    public static final String RECIPE_URL = ROOT_URL + "/recipes";
    public static final String ONLINE_ORDER_URL = ROOT_URL + "/orders";
    public static final String RECIPE_TOP_VIEW_URL =  "/view";
    public static final String RECIPE__TOP_NEW_URL =  "/new";
    public static final String ID_URL = "/{id}";
    public static final String SEARCH_URL = "/search";

    public static final String HTTP_STATUS = "http_status";

    public static final String SEARCH_TEXT = "search_text";
    public static final String SORT_BY = "sort_by";

    public static final String PAGE = "page";
    public static final String PER_PAGE = "per_page";
    public static final Integer DEFAULT_PAGE = 0;
    public static final Integer DEFAULT_PERPAGE = 25;

    public static final String COLLECTION_META_TYPE = "collection";

    public static final String ERROR_CODE_SPLIT = "#";
    public static final String QUESTION_MARK_SPLIT = "?";
    public static final String GENERAL_REC = "resource_error";

    public static final String BLANK_REC = "blank";
    public static final String INVALID_TYPE_REC = "invalid_type";
    public static final String INVALID_TYPE_REC_MSG = "Field may be invalid type.";
    public static final String INVALID_VALUE_REC_MSG = "Field may be invalid value.";
    /* RESOURCE ERROR CODES: RSC */


    public static final String UNKNOWN_REC = "unknown";

    public static final String INCORRECT_VALUE_REC = "incorrect_value";
    public static final String UNKNOWN_REC_MSG = "Field may be not found.";

    /**
     * Convert Map to URL string
     *
     * @param paras a map parameter
     * @return url string
     */
    public static String toUrlString(Map<String, String> paras)
    {
        StringBuilder sb = new StringBuilder();

        for (String key : paras.keySet()) {
            String value = paras.get(key);
            if (key != null && !key.isEmpty() && value != null && !value.isEmpty()) {
                if (sb.length() > 0) {
                    sb.append("&");
                }
                try {
                    key = URLEncoder.encode(key, CommonUtil.UTF_8);
                    value = URLEncoder.encode(value, CommonUtil.UTF_8);
                }
                catch (UnsupportedEncodingException e) {
                    throw new RuntimeException("This method requires UTF-8 encoding support", e);
                }
                sb.append(key).append("=").append(value);
            }
        }

        return sb.toString();
    }

    /**
     * Convert URL string to Map.
     *
     * @param queryString the query string.
     * @return url string.
     */
    public static Map<String, String> toMap(String queryString)
    {
        Map<String, String> paras = new HashMap<String, String>();
        if (queryString != null && !queryString.isEmpty()) {
            try {
                String payload = URLDecoder.decode(queryString, CommonUtil.UTF_8);
                String[] parameters = payload.split(CommonUtil.AND_SPLIT);
                for (String parameter : parameters) {
                    String[] kv = parameter.split(CommonUtil.EQUAL_SPLIT);
                    if (kv.length == 2) {
                        paras.put(kv[0], kv[1]);
                    }
                }
            }
            catch (Exception e) {

            }
        }

        return paras;
    }

    /**
     * Search base on search conditions.
     *
     * @param paras the search conditions: search text, sort, pagination and map of property name and value.
     * @return the list of data transfer objects in standard response of web API.
     */
    public static <T> EnvelopeResponse<T> search(Map<String, String> paras,
                                                 GenericService genericService, Class<T> dtoClass)
    {
        Map<String, String> orginalParas = new HashMap<String, String>();

        String searchText = null;
        String sort = null;
        Integer page = null;
        Integer perPage = null;

        if (paras != null && !paras.isEmpty()) {
            orginalParas = new HashMap<String, String>(paras);
            searchText = paras.get(ApiUtil.SEARCH_TEXT);
            paras.remove(ApiUtil.SEARCH_TEXT, searchText);

            sort = paras.get(ApiUtil.SORT_BY);
            paras.remove(ApiUtil.SORT_BY, sort);

            List<Errors> errors = new ArrayList<Errors>();
            try {
                String value = paras.get(ApiUtil.PAGE);
                if (value != null) {
                    page = Integer.parseInt(value);
                }
            }
            catch (Exception e) {
                errors.add(CommonUtil.toErrors(dtoClass.getSimpleName(), ApiUtil.PAGE,
                                               ApiUtil.INCORRECT_VALUE_REC, "Incorrect value",
                                               HttpStatus.UNPROCESSABLE_ENTITY.toString()));
            }
            finally {
                paras.remove(ApiUtil.PAGE);
            }

            try {
                String value = paras.get(ApiUtil.PER_PAGE);
                if (value != null) {
                    perPage = Integer.parseInt(value);
                }
            }
            catch (Exception e) {
                errors.add(CommonUtil.toErrors(dtoClass.getSimpleName(), ApiUtil.PER_PAGE,
                                               ApiUtil.INCORRECT_VALUE_REC, "Incorrect value",
                                               HttpStatus.UNPROCESSABLE_ENTITY.toString()));
            }
            finally {
                paras.remove(ApiUtil.PER_PAGE);
            }

            if (!errors.isEmpty()) {
                throw new ErrorsResponse.Builder().errors(errors).build();
            }
        }

        Page<T> dtoPage = genericService.get(paras, searchText, sort, page, perPage);

        return new EnvelopeResponse.Builder<T>()
            .items(dtoPage.getContents())
            .meta(buildMeta(dtoPage.getTotal(), orginalParas))
            .build();
    }

    /**
     * Build response meta of standard web API of search.
     *
     * @param total the total dto.
     * @param paras the search conditions.
     * @return the response meta of standard web API.
     */
    private static Meta buildMeta(Long total, Map<String, String> paras)
    {
        Meta.Builder metaBuilder = new Meta.Builder().type(ApiUtil.COLLECTION_META_TYPE)
            .count(total);

        Integer page = ApiUtil.DEFAULT_PAGE;
        try {
            page = Integer.parseInt(paras.get(ApiUtil.PAGE));
        }
        catch (NumberFormatException e) {
        }
        finally {
            paras.put(ApiUtil.PAGE, String.valueOf(page));
        }

        Integer perPage = ApiUtil.DEFAULT_PERPAGE;
        try {
            perPage = Integer.parseInt(paras.get(ApiUtil.PER_PAGE));
        }
        catch (NumberFormatException e) {
        }
        finally {
            paras.put(ApiUtil.PER_PAGE, String.valueOf(perPage));
        }

        Links.Builder linksBuilder = new Links.Builder();

        linksBuilder.self(ApiUtil.QUESTION_MARK_SPLIT + ApiUtil.toUrlString(paras));

        paras.put(ApiUtil.PAGE, "0");
        linksBuilder.firstPage(ApiUtil.QUESTION_MARK_SPLIT + ApiUtil.toUrlString(paras));

        Long last = total / perPage;
        paras.put(ApiUtil.PAGE, String.valueOf(last));
        linksBuilder.lastPage(ApiUtil.QUESTION_MARK_SPLIT + ApiUtil.toUrlString(paras));

        if ((page - 1) >= 0) {
            paras.put(ApiUtil.PAGE, String.valueOf(page - 1));
            linksBuilder.prevPage(ApiUtil.QUESTION_MARK_SPLIT + ApiUtil.toUrlString(paras));
        }

        if ((page + 1) <= last) {
            paras.put(ApiUtil.PAGE, String.valueOf(page + 1));
            linksBuilder.nextPage(ApiUtil.QUESTION_MARK_SPLIT + ApiUtil.toUrlString(paras));
        }

        return metaBuilder.links(linksBuilder.build()).build();

    }

}
