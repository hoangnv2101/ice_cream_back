package nashtech.training.iceCream.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


/**
 * This is application configuration.
 *
 * @author nvanhoang
 * @since 22/06/2018
 */

@Configuration
@ComponentScan(basePackages = { "nashtech.training.iceCream.rest" })
@ImportResource("classpath*:config/spring/ice-cream-context.xml")
public class ApplicationConfig
{

}
