package nashtech.training.iceCream.service.impl;

import nashtech.training.iceCream.domain.dao.OnlineOrderDao;
import nashtech.training.iceCream.domain.model.OnlineOrder;
import nashtech.training.iceCream.dto.OnlineOrderDto;
import nashtech.training.iceCream.helper.ObjectMapperHelper;
import nashtech.training.iceCream.service.OnineOrderService;

public class OnlineOrderServiceImpl  extends GenericServiceImpl<OnlineOrderDto, OnlineOrder, Integer> implements
        OnineOrderService {

    private final OnlineOrderDao onlineOrderDao;
    private final ObjectMapperHelper objectMapperHelper;

    /**
     * Generic constructor.
     *
     * @param objectMapperHelper the object mapper helper.
     * @param onlineOrderDao         the generic data access object.
     */
    public OnlineOrderServiceImpl(ObjectMapperHelper objectMapperHelper, OnlineOrderDao onlineOrderDao) {
        super(objectMapperHelper, onlineOrderDao);
        this.onlineOrderDao = onlineOrderDao;
        this.objectMapperHelper = objectMapperHelper;
    }
}
