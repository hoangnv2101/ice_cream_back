package nashtech.training.iceCream.service.impl;

import nashtech.training.iceCream.domain.dao.CustomerDao;
import nashtech.training.iceCream.domain.model.Customer;
import nashtech.training.iceCream.dto.CustomerDto;
import nashtech.training.iceCream.helper.ObjectMapperHelper;
import nashtech.training.iceCream.service.CustomerManagementService;

public class CustomerManagementServiceImpl extends GenericServiceImpl<CustomerDto, Customer, Integer> implements
        CustomerManagementService {

    private final CustomerDao customerDao;


    /**
     * Generic constructor.
     *
     * @param objectMapperHelper the object mapper helper.
     * @param customerDao         the customerDao access object.
     */
    public CustomerManagementServiceImpl(ObjectMapperHelper objectMapperHelper, CustomerDao customerDao) {
        super(objectMapperHelper, customerDao);
        this.customerDao = customerDao;
    }
}
