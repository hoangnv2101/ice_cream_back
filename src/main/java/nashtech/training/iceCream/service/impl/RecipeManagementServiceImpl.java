package nashtech.training.iceCream.service.impl;

import nashtech.training.iceCream.domain.dao.RecipeDao;
import nashtech.training.iceCream.domain.model.Recipe;
import nashtech.training.iceCream.dto.Page;
import nashtech.training.iceCream.dto.RecipeDto;
import nashtech.training.iceCream.helper.ObjectMapperHelper;
import nashtech.training.iceCream.service.RecipeManagementService;


import java.util.List;
import java.util.stream.Collectors;

public class RecipeManagementServiceImpl extends GenericServiceImpl<RecipeDto, Recipe, Integer> implements
        RecipeManagementService {

    private final RecipeDao recipeDao;
    private final ObjectMapperHelper objectMapperHelper;

    /**
     * Generic constructor.
     *
     * @param objectMapperHelper the object mapper helper.
     * @param recipeDao         the generic data access object.
     */
    public RecipeManagementServiceImpl(ObjectMapperHelper objectMapperHelper,  RecipeDao recipeDao) {
        super(objectMapperHelper, recipeDao);
        this.recipeDao = recipeDao;
        this.objectMapperHelper = objectMapperHelper;
    }


    @Override
    public Page<RecipeDto> getRecipeOrderByViewNumberDesc(int maxResult) {
        List<Recipe> recipes =  this.recipeDao.getRecipeOrderByViewNumberDesc(maxResult);

        List<RecipeDto> recipeDtos = recipes.stream().filter(e -> e != null)
                .map(e -> objectMapperHelper.map(e, RecipeDto.class))
                .collect(Collectors.toList());

        return  new Page<>(recipes.size(), recipeDtos);
    }

    @Override
    public Page<RecipeDto> getRecipesNewest(int maxResult) {
        List<Recipe> recipes =  this.recipeDao.getRecipesNewest(maxResult);

        List<RecipeDto> recipeDtos = recipes.stream().filter(e -> e != null)
                .map(e -> objectMapperHelper.map(e, RecipeDto.class))
                .collect(Collectors.toList());

        return new Page<>(recipes.size(), recipeDtos);
    }
}
