package nashtech.training.iceCream.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nashtech.training.iceCream.domain.dao.GenericDao;
import nashtech.training.iceCream.dto.Page;
import nashtech.training.iceCream.dto.VisitorType;
import nashtech.training.iceCream.dto.api.Errors;
import nashtech.training.iceCream.dto.api.ErrorsResponse;
import nashtech.training.iceCream.dto.api.HttpStatus;
import nashtech.training.iceCream.helper.ObjectMapperHelper;
import nashtech.training.iceCream.service.GenericService;
import nashtech.training.iceCream.util.ApiUtil;
import nashtech.training.iceCream.util.CommonUtil;
import nashtech.training.iceCream.visitor.Visitor;
import nashtech.training.iceCream.visitor.VisitorContext;
import nashtech.training.iceCream.visitor.impl.VisitorContextImpl;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This is generic implementation of {@link GenericService}
 *
 * @param <T>  the data transfer object.
 * @param <E>  the entity object.
 * @param <ID> the identifier.
 * @author nvanhoang
 * @since 18/06/2018
 */
public abstract class GenericServiceImpl<T extends Serializable, E extends Serializable, ID extends Serializable>
    implements GenericService<T, ID>
{
    private final Class<T> dtoClass;
    private final Class<E> entityClass;

    private final ObjectMapperHelper objectMapperHelper;
    private final GenericDao<E, ID> genericDao;
    private List<String> searchProperties;
    private List<String> editableProperties;

    private Map<Integer, Visitor> createVisitors = new HashMap<Integer, Visitor>();
    private Map<Integer, Visitor> updateVisitors = new HashMap<Integer, Visitor>();
    private Map<Integer, Visitor> deleteVisitors = new HashMap<Integer, Visitor>();

    /**
     * Generic constructor.
     *
     * @param objectMapperHelper the object mapper helper.
     * @param genericDao         the generic data access object.
     */
    public GenericServiceImpl(ObjectMapperHelper objectMapperHelper,
                              GenericDao<E, ID> genericDao)
    {
        this.objectMapperHelper = objectMapperHelper;
        this.genericDao = genericDao;

        Class genericClass = getClass();
        while (genericClass != null && !(genericClass
            .getGenericSuperclass() instanceof ParameterizedType)) {
            genericClass = genericClass.getSuperclass();
        }
        dtoClass = (Class<T>) ((ParameterizedType) genericClass.getGenericSuperclass())
            .getActualTypeArguments()[0];
        entityClass = (Class<E>) ((ParameterizedType) genericClass.getGenericSuperclass())
            .getActualTypeArguments()[1];
    }

    /**
     * Setter.
     *
     * @param searchProperties the search properties.
     */
    public void setSearchProperties(String searchProperties)
    {
        this.searchProperties = new ArrayList<String>(
            Arrays.asList(searchProperties.split(CommonUtil.COMMA_SPLIT)));
    }

    /**
     * Setter.
     *
     * @param editableProperties the search properties.
     */
    public void setEditableProperties(String editableProperties)
    {
        this.editableProperties = new ArrayList<String>(
            Arrays.asList(editableProperties.split(CommonUtil.COMMA_SPLIT)));
    }

    /**
     * Setter.
     *
     * @param createVisitors the list of visitors in order for create operation.
     */
    public void setCreateVisitors(Map<Integer, Visitor> createVisitors)
    {
        this.createVisitors = createVisitors;
    }

    /**
     * Setter.
     *
     * @param updateVisitors the list of visitors in order for update/edit operation.
     */
    public void setUpdateVisitors(Map<Integer, Visitor> updateVisitors)
    {
        this.updateVisitors = updateVisitors;
    }

    /**
     * Setter.
     *
     * @param deleteVisitors the list of visitors in order for delete operation.
     */
    public void setDeleteVisitors(
        Map<Integer, Visitor> deleteVisitors)
    {
        this.deleteVisitors = deleteVisitors;
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public Page<T> get(Map<String, String> paras, String searchText, String sort, Integer page,
                       Integer perPage)
    {
        validateSearchParas(paras);

        List<T> dtos = new ArrayList<T>();
        List<E> entities = new ArrayList<E>();

        Page<E> entityPage = search(paras, searchText, sort, page, perPage);

        entities.addAll(entityPage.getContents());

        dtos.addAll(
            entities.stream().filter(e -> e != null).map(e -> objectMapperHelper.map(e, dtoClass))
                .collect(Collectors.toList()));

        return new Page<T>(entityPage.getTotal(), dtos);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public T get(ID id)
    {
        T dto = null;
        E entity = genericDao.findById(id);
        if (entity != null) {
            dto = objectMapperHelper.map(entity, dtoClass);
        }
        return dto;
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public T create(T dto)
    {
        T createdDto = null;

        VisitorContext context = new VisitorContextImpl();
        context.put(CommonUtil.VISITING_TYPE, VisitorType.CREATE);

        // map dto to entity
        E entity = objectMapperHelper.map(dto, entityClass);

        // pre-create the entity
        visitBeforeCreate(dto, entity, context);

        // create the entity
        entity = genericDao.save(entity);

        if (entity != null) {
            createdDto = objectMapperHelper.map(entity, dtoClass);

            // post-create the entity
            visitAfterCreate(createdDto, entity, context);
        }
        return createdDto;
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public T update(ID id, T dto)
    {
        return update(id, dto, null);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public T update(ID id, String jsonPayload)
    {
        T dto = null;
        List<String> nullProperties = new ArrayList<String>();
        List<Errors> errors = new ArrayList<Errors>();

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> map = objectMapper
                .readValue(jsonPayload, new TypeReference<Map<String, Object>>()
                {
                });

            map.entrySet().stream().forEach(e -> {
                String propertyName = e.getKey();
                if (e.getValue() == null) {
                    nullProperties.add(propertyName);
                }
                else {
                    if (editableProperties != null && !editableProperties.contains(propertyName)) {
                        errors.add(CommonUtil.toErrors(dtoClass.getSimpleName(), propertyName,
                                                       ApiUtil.UNKNOWN_REC,
                                                       ApiUtil.UNKNOWN_REC_MSG,
                                                       HttpStatus.UNPROCESSABLE_ENTITY.toString()));
                    }
                }
            });

            if (errors != null && !errors.isEmpty()) {
                throw new ErrorsResponse.Builder().errors(errors).build();
            }

            dto = objectMapper.readValue(jsonPayload, dtoClass);
        }
        catch (IOException e) {
            errors.add(CommonUtil.toErrors(dtoClass.getSimpleName(), "unknown",
                                           ApiUtil.UNKNOWN_REC,
                                           e.getMessage(),
                                           HttpStatus.UNPROCESSABLE_ENTITY.toString()));
        }

        if (errors != null && !errors.isEmpty()) {
            throw new ErrorsResponse.Builder().errors(errors).build();
        }

        return update(id, dto, nullProperties);
    }

    private T update(ID id, T dto, List<String> nullProperties)
    {
        T updatedDto = null;
        final E entity = genericDao.findById(id);
        if (entity != null) {
            VisitorContext context = new VisitorContextImpl();
            context.put(CommonUtil.VISITING_TYPE, VisitorType.UPDATE);

            // pre-edit the entity
            visitBeforeUpdate(dto, entity, context);

            if (nullProperties != null && !nullProperties.isEmpty()) {
                final BeanWrapper dtoWrapper = new BeanWrapperImpl(entity);
                nullProperties.stream().forEach(p -> {
                    dtoWrapper.setPropertyValue(p, null);
                });
            }

            // edit the entity
            final E updated = genericDao.saveOrUpdate(entity);

            if (updated != null) {
                updatedDto = objectMapperHelper.map(updated, dtoClass);

                // post-edit the entity
                visitAfterUpdate(updatedDto, updated, context);
            }
        }
        return updatedDto;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(ID id)
    {
        genericDao.deleteById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Collection<ID> ids)
    {
        genericDao.deleteById(ids);
    }


    /**
     * Validate search conditions base on pre-defined a list of search properties.
     *
     * @param paras the search conditions.
     */
    private void validateSearchParas(Map<String, String> paras)
    {
        if (searchProperties != null && paras != null && !paras.isEmpty()) {
            List<Errors> errors = paras.keySet().stream().filter(k -> !searchProperties.contains(k))
                .map(k -> {
                    return CommonUtil.toErrors(dtoClass.getSimpleName(), k,
                                               ApiUtil.UNKNOWN_REC,
                                               ApiUtil.UNKNOWN_REC_MSG,
                                               HttpStatus.UNPROCESSABLE_ENTITY.toString());

                }).collect(Collectors.toList());

            if (errors != null && !errors.isEmpty()) {
                throw new ErrorsResponse.Builder().errors(errors).build();
            }
        }
    }

    /**
     * This will be overridden if want to provide custom searching.
     *
     * @param paras the search parameters.
     * @return a list of entities.
     */
    private Page<E> search(Map<String, String> paras, String searchText, String sort,
                           Integer page,
                           Integer perPage)
    {
        return genericDao.search(paras, searchText, sort, page, perPage);
    }

    /**
     * Invoke additional functions before creating the entity.
     *
     * @param dto     the data transfer object.
     * @param entity  the entity object.
     * @param context the visitor context.
     */
    private void visitBeforeCreate(final T dto, final E entity, VisitorContext context)
    {
        createVisitors.entrySet().stream().sorted(Map.Entry.comparingByKey())
            .forEachOrdered(e -> e.getValue().before(dto, entity, context));
    }

    /**
     * Invoke addtional functions after creating the entity.
     *
     * @param dto     the data transfer object.
     * @param entity  the entity object.
     * @param context the visitor context.
     */
    private void visitAfterCreate(final T dto, final E entity, VisitorContext context)
    {
        createVisitors.entrySet().stream()
            .sorted(Collections.reverseOrder(Map.Entry.comparingByKey()))
            .forEachOrdered(e -> e.getValue().after(dto, entity, context));
    }

    /**
     * Invoke additional functions before updating/editing the entity.
     *
     * @param dto     the data transfer object.
     * @param entity  the entity object.
     * @param context the visitor context.
     */
    private void visitBeforeUpdate(final T dto, final E entity, VisitorContext context)
    {
        updateVisitors.entrySet().stream().sorted(Map.Entry.comparingByKey())
            .forEachOrdered(e -> e.getValue().before(dto, entity, context));
    }

    /**
     * Invoke additional functions after updating/editing the entity.
     *
     * @param dto     the data transfer object.
     * @param entity  the entity object.
     * @param context the visitor context.
     */
    private void visitAfterUpdate(final T dto, final E entity, VisitorContext context)
    {
        updateVisitors.entrySet().stream()
            .sorted(Collections.reverseOrder(Map.Entry.comparingByKey()))
            .forEachOrdered(e -> e.getValue().after(dto, entity, context));
    }

    /**
     * Invoke additional functions before deleting the entity.
     *
     * @param id      the id.
     * @param entity  the entity object.
     * @param context the visitor context.
     */
    private void visitBeforeDelete(final ID id, final E entity, VisitorContext context)
    {
        deleteVisitors.entrySet().stream().sorted(Map.Entry.comparingByKey())
            .forEachOrdered(e -> e.getValue().before(id, entity, context));
    }

    /**
     * Invoke additional functions after deleting the entity.
     *
     * @param id      the id.
     * @param entity  the entity object.
     * @param context the visitor context.
     */
    private void visitAfterDelete(final ID id, final E entity, VisitorContext context)
    {
        deleteVisitors.entrySet().stream()
            .sorted(Collections.reverseOrder(Map.Entry.comparingByKey()))
            .forEachOrdered(e -> e.getValue().after(id, entity, context));
    }
}
