package nashtech.training.iceCream.service;

import nashtech.training.iceCream.dto.Page;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * This defines common functions of generic service interface.
 *
 * @param <T>  The data transfer object (DTO).
 * @param <ID> The identifier of the dto.
 * @author nvanhoang
 * @since 14/06/2018
 */
public interface GenericService<T extends Serializable, ID extends Serializable>
{
    /**
     * Get all data transfer objects base on search condition, sort and pagination.
     *
     * @param paras      the parameter map of property name and value.
     * @param searchText the search text.
     * @param sort       the sort properties.
     * @param page       the page index.
     * @param perPage    the page size.
     * @return the list of data transfer objects.
     */
    Page<T> get(Map<String, String> paras, String searchText, String sort, Integer page,
                Integer perPage);

    /**
     * Get a data transfer object by id.
     *
     * @param id the id.
     * @return the data transfer object.
     */
    T get(ID id);

    /**
     * Create the entity base on the data transfer object.
     *
     * @param dto the data transfer object.
     * @return the data transfer object of the created entity.
     */
    T create(T dto);

    /**
     * Update the entity base on the data transfer object.
     *
     * @param id  the id.
     * @param dto the data transfer object.
     * @return the data transfer object of the updated entity.
     */
    T update(ID id, T dto);

    /**
     * Update the entity base on the data transfer object.
     *
     * @param id          the id.
     * @param jsonPayload the json payload.
     * @return the data transfer object of the updated entity.
     */
    T update(ID id, String jsonPayload);

    /**
     * Delete the entity by id.
     *
     * @param id the id.
     */
    void delete(ID id);

    /**
     * Delete the entities by collection of ids.
     *
     * @param ids the ids.
     */
    void delete(Collection<ID> ids);

}
