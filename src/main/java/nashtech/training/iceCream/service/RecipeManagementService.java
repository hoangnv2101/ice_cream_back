package nashtech.training.iceCream.service;

import nashtech.training.iceCream.dto.Page;
import nashtech.training.iceCream.dto.RecipeDto;

import java.util.List;

/**
 * This is account management service extended {@link GenericService}
 *
 * @author nvanhoang
 * @since 23/06/2018
 */
public interface RecipeManagementService  extends GenericService<RecipeDto, Integer>{

    /**
     * Find entities order by view number.
     *
     * @param maxResult  max result to return.
     * @return the list of entities.
     */
    Page<RecipeDto> getRecipeOrderByViewNumberDesc(int maxResult);

    /**
     * Find entities order by id.
     *
     * @param maxResult  max result to return.
     * @return the list of entities.
     */
    Page<RecipeDto> getRecipesNewest(int maxResult);
}
