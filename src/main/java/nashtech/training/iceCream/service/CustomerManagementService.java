package nashtech.training.iceCream.service;

import nashtech.training.iceCream.dto.CustomerDto;

/**
 * This is account management service extended {@link GenericService}
 *
 * @author nvanhoang
 * @since 23/06/2018
 */
public interface CustomerManagementService extends GenericService<CustomerDto, Integer>{
}
