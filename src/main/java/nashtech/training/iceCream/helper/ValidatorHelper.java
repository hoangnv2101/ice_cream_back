package nashtech.training.iceCream.helper;

import javax.validation.ConstraintViolation;
import java.util.Set;

/**
 * This is validation helper.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public interface ValidatorHelper
{
    /**
     * Validate the object.
     *
     * @param object  the object.
     * @param klasses the classes.
     * @param <T>
     * @return the set of contraint violations.
     */
    <T> Set<ConstraintViolation<T>> validate(T object, Class... klasses);
}
