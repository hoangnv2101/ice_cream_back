package nashtech.training.iceCream.helper.impl;


import nashtech.training.iceCream.dto.exception.ObjectMappingException;
import nashtech.training.iceCream.helper.ObjectMapperHelper;
import org.dozer.Mapper;

/**
 * This implements {@link ObjectMapperHelper} and wraps dozer mapping.
 *
 * @author nvanhoang
 * @since 22/06/2018
 */
public class DozerMapperHelperImpl implements ObjectMapperHelper
{
    private final Mapper dozerMapper;

    /**
     * Constructor.
     *
     * @param dozerMapper the dozer mapping.
     */
    public DozerMapperHelperImpl(Mapper dozerMapper)
    {
        this.dozerMapper = dozerMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T map(Object source, Class<T> destClass) throws ObjectMappingException
    {
        if (source.getClass().isAssignableFrom(destClass)) {
            return (T) source;
        }
        return dozerMapper.map(source, destClass);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void map(Object source, Object destination) throws ObjectMappingException
    {
        dozerMapper.map(source, destination);
    }
}
