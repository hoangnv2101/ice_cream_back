package nashtech.training.iceCream.helper.impl;

import nashtech.training.iceCream.helper.ValidatorHelper;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * This implements {@link ValidatorHelper}.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public class HibernateValidatorHelper implements ValidatorHelper
{
    private Validator validator;

    /**
     * Constructor.
     */
    public HibernateValidatorHelper()
    {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> Set<ConstraintViolation<T>> validate(T object, Class... klasses)
    {
        return validator.validate(object, klasses);
    }
}
