package nashtech.training.iceCream.helper;


import nashtech.training.iceCream.dto.exception.ObjectMappingException;

/**
 * This is object mapper helper.
 *
 * @author nvanhoang
 * @since 15/06/2018
 */
public interface ObjectMapperHelper
{
    /**
     * Constructs new instance of destinationClass and performs mapping between from source.
     *
     * @param source    the source object.
     * @param destClass the destination class.
     * @param <T>
     * @return the new instance of destination class.
     * @throws ObjectMappingException the object mapping exception.
     */
    <T> T map(Object source, Class<T> destClass) throws ObjectMappingException;

    /**
     * Performs mapping between source and destination objects.
     *
     * @param source      the source object.
     * @param destination the destination object.
     * @throws ObjectMappingException the object mapping exception.
     */
    void map(Object source, Object destination) throws ObjectMappingException;
}
