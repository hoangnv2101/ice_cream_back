package nashtech.training.iceCream.visitor;

/**
 * Visitor interface defines additional functions for the dto object and the entity object before/after the main function.
 *
 * @param <T> the data transfer object (DTO).
 * @param <E> the entity object.
 * @author nvanhoang
 * @since 19/06/2018
 */
public interface Visitor<T, E>
{
    /**
     * This is an additional function before the main function.
     *
     * @param dto     the data transfer object.
     * @param entity  the entity object.
     * @param context the visitor context.
     */
    void before(final T dto, final E entity, VisitorContext context);

    /**
     * This is an additional function after the main function.
     *
     * @param dto     the data transfer object.
     * @param entity  the entity object.
     * @param context the visitor context.
     */
    void after(final T dto, final E entity, VisitorContext context);
}
