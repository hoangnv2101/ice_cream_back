package nashtech.training.iceCream.visitor;

/**
 * This is visitor context used to exchange information between visitors.
 *
 * @author nvanhoang
 * @since 19/06/2018
 */
public interface VisitorContext
{
    /**
     * Put information into the context.
     *
     * @param key   the key.
     * @param value the value.
     */
    void put(String key, Object value);

    /**
     * Get the information from the context.
     *
     * @param key the key.
     * @return the value from the context if existing. Otherwise, return null.
     */
    Object get(String key);
}
