package nashtech.training.iceCream.visitor.impl;

import nashtech.training.iceCream.dto.api.Errors;
import nashtech.training.iceCream.dto.api.ErrorsResponse;
import nashtech.training.iceCream.dto.api.HttpStatus;
import nashtech.training.iceCream.helper.ValidatorHelper;
import nashtech.training.iceCream.util.ApiUtil;
import nashtech.training.iceCream.util.CommonUtil;
import nashtech.training.iceCream.visitor.Visitor;
import nashtech.training.iceCream.visitor.VisitorContext;
import javax.validation.ConstraintViolation;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * This is for entity validation before saving or updating the entity.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public class EntityValidationVisitor extends AbstractVisitor<Object, Object>
    implements Visitor<Object, Object>
{
    private final ValidatorHelper validatorHelper;

    /**
     * Constructor.
     *
     * @param validatorHelper the validation helper.
     */
    public EntityValidationVisitor(ValidatorHelper validatorHelper)
    {
        this.validatorHelper = validatorHelper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void before(final Object dto, final Object entity, VisitorContext context)
    {
        List<Errors> errors = new ArrayList<Errors>();
        Set<ConstraintViolation<Object>> violations = validatorHelper.validate(entity);
        if (violations != null && !violations.isEmpty()) {
            for (ConstraintViolation<Object> violation : violations) {
                String code = ApiUtil.GENERAL_REC;
                String message = violation.getMessage();
                String[] messages = message.split(ApiUtil.ERROR_CODE_SPLIT);
                if (messages.length == 2) {
                    code = messages[0];
                    message = messages[1];
                }

                errors.add(CommonUtil.toErrors(violation.getRootBeanClass().getSimpleName(),
                                               violation.getPropertyPath().toString(), code,
                                               message,
                                               HttpStatus.UNPROCESSABLE_ENTITY.toString()));
            }
        }

        if (!errors.isEmpty()) {
            throw new ErrorsResponse.Builder().errors(errors).build();
        }
    }
}
