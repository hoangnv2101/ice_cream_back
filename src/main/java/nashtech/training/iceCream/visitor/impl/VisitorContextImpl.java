package nashtech.training.iceCream.visitor.impl;

import nashtech.training.iceCream.visitor.VisitorContext;

import java.util.HashMap;
import java.util.Map;

/**
 * This is simple visitor context implementation.
 *
 * @author nvanhoang
 * @since 19/06/2018
 */
public class VisitorContextImpl implements VisitorContext
{
    private Map<String, Object> context = new HashMap<String, Object>();

    /**
     * {@inheritDoc}
     */
    @Override
    public void put(String key, Object value)
    {
        context.put(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object get(String key)
    {
        return context.get(key);
    }
}
