package nashtech.training.iceCream.visitor.impl;


import nashtech.training.iceCream.helper.ObjectMapperHelper;
import nashtech.training.iceCream.visitor.Visitor;
import nashtech.training.iceCream.visitor.VisitorContext;

/**
 * This maps the dto object to the entity object.
 *
 * @author nvhoang
 * @since 19/06/2018
 */
public class ObjectMapperVisitor extends AbstractVisitor<Object, Object>
    implements Visitor<Object, Object>
{
    private final ObjectMapperHelper objectMapperHelper;

    /**
     * Constructor.
     *
     * @param objectMapperHelper the object mapper helper.
     */
    public ObjectMapperVisitor(ObjectMapperHelper objectMapperHelper)
    {
        this.objectMapperHelper = objectMapperHelper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void before(final Object dto, final Object entity, VisitorContext context)
    {
        objectMapperHelper.map(dto, entity);
    }
}
