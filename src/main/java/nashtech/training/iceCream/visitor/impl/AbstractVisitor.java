package nashtech.training.iceCream.visitor.impl;


import nashtech.training.iceCream.visitor.Visitor;
import nashtech.training.iceCream.visitor.VisitorContext;

/**
 * This is abstract class implemented default behaviours.
 *
 * @author nvahoang
 * @since 19/06/2018
 */
public abstract class AbstractVisitor<T, E> implements Visitor<T, E>
{
    /**
     * {@inheritDoc}
     */
    @Override
    public void before(final T dto, final E entity, VisitorContext context)
    {
        // by default, do nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void after(final T dto, final E entity, VisitorContext context)
    {
        // by default, do nothing
    }
}
