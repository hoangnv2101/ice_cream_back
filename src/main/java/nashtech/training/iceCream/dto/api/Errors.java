package nashtech.training.iceCream.dto.api;

/**
 * This is standard errors of web API.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public class Errors
{
    private final Error error;
    private final Meta meta;

    public Errors(Error error, Meta meta)
    {
        this.error = error;
        this.meta = meta;
    }

    public Error getError()
    {
        return error;
    }

    public Meta getMeta()
    {
        return meta;
    }

    @Override
    public String toString() {
        return "Errors [error=" + error + ", meta=" + meta + "]";
    }
}
