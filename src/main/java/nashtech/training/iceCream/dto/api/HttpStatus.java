package nashtech.training.iceCream.dto.api;


import nashtech.training.iceCream.util.ApiUtil;

/**
 * This is http status.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public enum HttpStatus
{
    BAD_REQUEST(400, "Bad Request"),
    UNPROCESSABLE_ENTITY(422, "Unprocessable Entity");

    private Integer code;
    private String message;

    private HttpStatus(Integer code, String message)
    {
        this.code = code;
        this.message = message;
    }

    @Override public String toString()
    {
        return code + ApiUtil.ERROR_CODE_SPLIT + message;
    }
}
