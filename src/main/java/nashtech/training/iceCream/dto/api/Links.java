package nashtech.training.iceCream.dto.api;

/**
 * This is standard meta pagination links of web API.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public class Links
{
    private final String self;
    private final String firstPage;
    private final String prevPage;
    private final String nextPage;
    private final String lastPage;
    private final String moreInfo;

    public Links(String self, String firstPage, String prevPage, String nextPage, String lastPage,
                 String moreInfo)
    {
        this.self = self;
        this.firstPage = firstPage;
        this.prevPage = prevPage;
        this.nextPage = nextPage;
        this.lastPage = lastPage;
        this.moreInfo = moreInfo;
    }

    public String getSelf()
    {
        return self;
    }

    public String getFirstPage()
    {
        return firstPage;
    }

    public String getPrevPage()
    {
        return prevPage;
    }

    public String getNextPage()
    {
        return nextPage;
    }

    public String getLastPage()
    {
        return lastPage;
    }

    public String getMoreInfo()
    {
        return moreInfo;
    }

    public static class Builder
    {
        private String self;
        private String firstPage;
        private String prevPage;
        private String nextPage;
        private String lastPage;
        private String moreInfo;

        public Builder()
        {
        }

        public Builder self(final String self)
        {
            this.self = self;
            return this;
        }

        public Builder firstPage(final String firstPage)
        {
            this.firstPage = firstPage;
            return this;
        }

        public Builder prevPage(final String prevPage)
        {
            this.prevPage = prevPage;
            return this;
        }

        public Builder nextPage(final String nextPage)
        {
            this.nextPage = nextPage;
            return this;
        }

        public Builder lastPage(final String lastPage)
        {
            this.lastPage = lastPage;
            return this;
        }

        public Builder moreInfo(final String moreInfo)
        {
            this.moreInfo = moreInfo;
            return this;
        }

        public Links build()
        {
            return new Links(this.self, this.firstPage, this.prevPage, this.nextPage, this.lastPage,
                             this.moreInfo);
        }
    }

    @Override
    public String toString() {
        return "Links [self=" + self + ", firstPage=" + firstPage + ", prevPage=" + prevPage + ", nextPage=" + nextPage
                + ", lastPage=" + lastPage + ", moreInfo=" + moreInfo + "]";
    }
}
