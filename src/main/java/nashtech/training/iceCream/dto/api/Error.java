package nashtech.training.iceCream.dto.api;

/**
 * This is standard error of web API.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public class Error
{
    private final String code;
    private final String message;
    private final String details;
    private final String resource;
    private final String field;

    public Error(String resource, String field, String code, String message, String details)
    {
        this.resource = resource;
        this.field = field;
        this.code = code;
        this.message = message;
        this.details = details;
    }

    public String getCode()
    {
        return code;
    }

    public String getMessage()
    {
        return message;
    }

    public String getDetails()
    {
        return details;
    }

    public String getResource()
    {
        return resource;
    }

    public String getField()
    {
        return field;
    }

    @Override
    public String toString() {
        return "Error [code=" + code + ", message=" + message + ", details=" + details + ", resource=" + resource
                + ", field=" + field + "]";
    }
}
