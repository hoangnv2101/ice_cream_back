package nashtech.training.iceCream.dto.api;

import java.util.ArrayList;
import java.util.List;

/**
 * This is standard error response of web API.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public class ErrorsResponse extends RuntimeException
{
    private final List<Errors> errors;
    private final Meta meta;

    public ErrorsResponse(List<Errors> errors, Meta meta)
    {
        this.errors = errors;
        this.meta = meta;
    }

    public List<Errors> getErrors()
    {
        return errors;
    }

    public Meta getMeta()
    {
        return meta;
    }

    @Override
    public String toString() {
        final int maxLen = 3;
        return "ErrorsResponse [errors=" + (errors != null ? errors.subList(0, Math.min(errors.size(), maxLen)) : null)
                + ", meta=" + meta + "]";
    }

    public static class Builder
    {
        private List<Errors> errors = new ArrayList<Errors>();
        ;
        private Meta meta;

        public Builder()
        {
        }

        public Builder errors(final List<Errors> errors)
        {
            this.errors.addAll(errors);
            return this;
        }

        public Builder errors(final Errors errors)
        {
            this.errors.add(errors);
            return this;
        }

        public Builder meta(final Meta meta)
        {
            this.meta = meta;
            return this;
        }

        public ErrorsResponse build()
        {
            return new ErrorsResponse(this.errors, this.meta);
        }
    }
}
