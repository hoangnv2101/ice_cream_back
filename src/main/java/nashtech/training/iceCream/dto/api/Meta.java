package nashtech.training.iceCream.dto.api;

/**
 * This is standard meta of web API.
 *
 * @author nvanhoang
 * @since 20/06/2018
 */
public class Meta
{
    private final String type;
    private final Long count;
    private final String httpStatus;
    private final String logref;
    private final Links links;

    public Meta(String type, Long count, String httpStatus, String logref, Links links)
    {
        this.type = type;
        this.count = count;
        this.httpStatus = httpStatus;
        this.logref = logref;
        this.links = links;
    }

    public String getType()
    {
        return type;
    }

    public Long getCount()
    {
        return count;
    }

    public String getHttpStatus()
    {
        return httpStatus;
    }

    public String getLogref()
    {
        return logref;
    }

    public Links getLinks()
    {
        return links;
    }

    @Override
    public String toString() {
        return "Meta [type=" + type + ", count=" + count + ", httpStatus=" + httpStatus + ", logref=" + logref
                + ", links=" + links + "]";
    }

    public static class Builder
    {
        private String type;
        private Long count;
        private String httpStatus;
        private String logref;
        private Links links;

        public Builder()
        {
        }

        public Builder type(final String type)
        {
            this.type = type;
            return this;
        }

        public Builder count(final Long count)
        {
            this.count = count;
            return this;
        }

        public Builder httpStatus(final String httpStatus)
        {
            this.httpStatus = httpStatus;
            return this;
        }

        public Builder logref(final String logref)
        {
            this.logref = logref;
            return this;
        }

        public Builder links(final Links links)
        {
            this.links = links;
            return this;
        }

        public Meta build()
        {
            return new Meta(this.type, this.count, this.httpStatus, this.logref, this.links);
        }
    }
}
