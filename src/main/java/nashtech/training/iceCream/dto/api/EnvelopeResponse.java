package nashtech.training.iceCream.dto.api;

import java.util.ArrayList;
import java.util.List;

/**
 * This is standard envelope response of web API.
 *
 * @param <T>
 * @author nvanhoang
 * @since 22/06/2018
 */
public class EnvelopeResponse<T>
{
    private final List<T> items;
    private final T data;
    private final List<Errors> errors;
    private final Error error;
    private final Meta meta;

    public EnvelopeResponse(final List<T> items, final T data, final List<Errors> errors,
                            final Error error, final Meta meta)
    {
        this.items = items;
        this.data = data;
        this.errors = errors;
        this.error = error;
        this.meta = meta;
    }

    public List<T> getItems()
    {
        return items;
    }

    public T getData()
    {
        return data;
    }

    public List<Errors> getErrors()
    {
        return errors;
    }

    public Error getError()
    {
        return error;
    }

    public Meta getMeta()
    {
        return meta;
    }

    public static class Builder<T>
    {
        private List<T> items = new ArrayList<T>();
        private T data;
        private List<Errors> errors = new ArrayList<Errors>();
        private Error error;
        private Meta meta;

        public Builder()
        {
        }

        public Builder items(final List<T> items)
        {
            this.items.addAll(items);
            return this;
        }

        public Builder data(final T data)
        {
            this.data = data;
            return this;
        }

        public Builder errors(final List<Errors> errors)
        {
            this.errors.addAll(errors);
            return this;
        }

        public Builder errors(final Errors errors)
        {
            this.errors.add(errors);
            return this;
        }

        public Builder error(final Error error)
        {
            this.error = error;
            return this;
        }

        public Builder meta(final Meta meta)
        {
            this.meta = meta;
            return this;
        }

        public EnvelopeResponse<T> build()
        {
            return new EnvelopeResponse<T>(this.items, this.data, this.errors, this.error,
                                           this.meta);
        }
    }

    @Override
    public String toString() {
        final int maxLen = 3;
        return "EnvelopeResponse [items=" + (items != null ? items.subList(0, Math.min(items.size(), maxLen)) : null)
                + ", data=" + data + ", errors="
                + (errors != null ? errors.subList(0, Math.min(errors.size(), maxLen)) : null) + ", error=" + error
                + ", meta=" + meta + "]";
    }
}
