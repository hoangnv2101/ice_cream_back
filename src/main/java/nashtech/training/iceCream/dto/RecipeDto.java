package nashtech.training.iceCream.dto;

import java.io.Serializable;
import java.util.Date;

public class RecipeDto implements Serializable
{
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    private String image;
    private String description;
    private String details;
    private String author;
    private int viewNumber;
    private Date uploadDate;
    private Boolean enablestatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getViewNumber() {
        return viewNumber;
    }

    public void setViewNumber(int viewNumber) {
        this.viewNumber = viewNumber;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Boolean getEnablestatus() {
        return enablestatus;
    }

    public void setEnablestatus(Boolean enablestatus) {
        this.enablestatus = enablestatus;
    }
}
