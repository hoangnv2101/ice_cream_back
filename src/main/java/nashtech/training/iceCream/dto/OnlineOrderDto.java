package nashtech.training.iceCream.dto;
import java.io.Serializable;
import java.util.Date;

public class OnlineOrderDto implements Serializable
{
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String name;
    private String email;
    private String contact;
    private String address;
    private Double bookCost;
    private String payingOption;
    private Date orderDate;
    private Boolean status;

    public Integer getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getBookCost() {
        return bookCost;
    }

    public void setBookCost(Double bookCost) {
        this.bookCost = bookCost;
    }

    public String getPayingOption() {
        return payingOption;
    }

    public void setPayingOption(String payingOption) {
        this.payingOption = payingOption;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
