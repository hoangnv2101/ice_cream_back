package nashtech.training.iceCream.dto.exception;

/**
 * This is object mapping exception.
 *
 * @author nvanhoang
 * @since 06/06/2018
 */
public class ObjectMappingException extends RuntimeException
{
    /**
     * Constructor.
     *
     * @param message the message.
     */
    public ObjectMappingException(String message)
    {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param message the message.
     * @param cause   the cause.
     */
    public ObjectMappingException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Constructor.
     *
     * @param message the message.
     */
    public ObjectMappingException(Throwable message)
    {
        super(message);
    }
}
