package nashtech.training.iceCream.dto;

import java.util.List;

/**
 * Created by nvanhoang on 18/06/2018.
 */
public class Page<T>
{
    private final long total;
    private final List<T> contents;

    public Page(long total, List<T> contents)
    {
        this.total = total;
        this.contents = contents;
    }

    public long getTotal()
    {
        return total;
    }

    public List<T> getContents()
    {
        return contents;
    }
}
