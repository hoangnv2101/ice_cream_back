package nashtech.training.iceCream.dto;

/**
 * Created by nvanhoang on 20/06/2018.
 */
public enum VisitorType
{
    ALL("all"),
    CREATE("create"),
    READ("read"),
    UPDATE("update"),
    DELETE("delete");

    private String value;

    private VisitorType(String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return value;
    }

    public String getValue()
    {
        return value;
    }
}
