package nashtech.training.iceCream.rest;

import nashtech.training.iceCream.dto.api.EnvelopeResponse;
import nashtech.training.iceCream.dto.api.Meta;
import nashtech.training.iceCream.service.GenericService;
import nashtech.training.iceCream.util.ApiUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Map;

/**
 * This defines common functions (GET, POST, PUT/PATH, DELETE) of generic REST service (web API implementation).
 *
 * @param <T>  the data transfer object.
 * @param <ID> the identifier.
 * @author nvanhoang
 * @since 20/06/2018
 */
@CrossOrigin
public abstract class GenericController<T extends Serializable, ID extends Serializable>
{
    private final Class<T> dtoClass;
    private final GenericService<T, ID> genericService;

    /**
     * Constructor.
     *
     * @param genericService {@link GenericService}.
     */
    public GenericController(GenericService<T, ID> genericService)
    {
        this.genericService = genericService;

        Class genericClass = getClass();
        while (genericClass != null && !(genericClass
            .getGenericSuperclass() instanceof ParameterizedType)) {
            genericClass = genericClass.getSuperclass();
        }
        dtoClass = (Class<T>) ((ParameterizedType) genericClass.getGenericSuperclass())
            .getActualTypeArguments()[0];
    }

    /**
     * Get (Search) by the HTTP Get base on the HTTP request parameters.
     *
     * @return return all if there is no the request parameters. Otherwise, return a list of data transfer object base on the HTTP request parameters. The result is in standard response of web API.
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EnvelopeResponse> get(HttpServletRequest request) throws IOException
    {
        HttpHeaders headers = new HttpHeaders();
        headers.add(ApiUtil.HTTP_STATUS, String.valueOf(HttpStatus.OK.value()));

        EnvelopeResponse<T> body = ApiUtil.<T>search(ApiUtil.toMap(request.getQueryString()),
                                                     genericService, dtoClass);

        return new ResponseEntity(body, headers, HttpStatus.OK);
    }

    /**
     * Post search (/search) by the HTTP POST base on the HTTP body data.
     *
     * @param paras the HTTP body data.
     * @return return all if there is no the HTTP body data. Otherwise, return a list of data transfer object base on the HTTP body data. The result is in standard response of web API.
     */
    @RequestMapping(value = ApiUtil.SEARCH_URL, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EnvelopeResponse> postSearch(
        @RequestBody(required = false) Map<String, String> paras)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.add(ApiUtil.HTTP_STATUS, String.valueOf(HttpStatus.OK.value()));

        EnvelopeResponse<T> body = ApiUtil.<T>search(paras, genericService, dtoClass);

        return new ResponseEntity(body, headers, HttpStatus.OK);
    }

    /**
     * Get (/{id}) by the HTTP Get.
     *
     * @param id the identifier.
     * @return the data transfer object in standard response of web API.
     */
    @RequestMapping(value = ApiUtil.ID_URL, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EnvelopeResponse> get(@PathVariable ID id)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.add(ApiUtil.HTTP_STATUS, String.valueOf(HttpStatus.OK.value()));

        EnvelopeResponse<T> body = new EnvelopeResponse.Builder<T>().data(genericService.get(id))
            .meta(new Meta.Builder().type(dtoClass.getSimpleName()).build()).build();

        return new ResponseEntity(body, headers, HttpStatus.OK);
    }

    /**
     * Create by the HTTP POST.
     *
     * @param dto the data transfer object.
     * @return the created data transfer object in standard response of web API.
     */
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EnvelopeResponse> post(@RequestBody(required = true) T dto)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.add(ApiUtil.HTTP_STATUS, String.valueOf(HttpStatus.CREATED.value()));

        EnvelopeResponse<T> body = new EnvelopeResponse.Builder<T>()
            .data(genericService.create(dto))
            .meta(new Meta.Builder().type(dtoClass.getSimpleName()).build()).build();

        return new ResponseEntity(body, headers, HttpStatus.CREATED);
    }

    /**
     * Mark as deleted by the HTTP DELETE.
     *
     * @param id the identifier.
     */
    @RequestMapping(value = ApiUtil.ID_URL, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> delete(@PathVariable ID id)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.add(ApiUtil.HTTP_STATUS, String.valueOf(HttpStatus.NO_CONTENT.value()));

        return new ResponseEntity(headers, HttpStatus.NO_CONTENT);
    }

    /**
     * Update/Edit by the HTTP PUT.
     *
     * @param id  the identifier.
     * @param dto the data transfer object.
     * @return the updated/edited data transfer object in standard response of web API.
     */
    @RequestMapping(value = ApiUtil.ID_URL, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EnvelopeResponse> put(@PathVariable ID id,
                                                @RequestBody(required = true) T dto)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.add(ApiUtil.HTTP_STATUS, String.valueOf(HttpStatus.OK.value()));

        EnvelopeResponse<T> body = new EnvelopeResponse.Builder<T>()
            .data(genericService.update(id, dto))
            .meta(new Meta.Builder().type(dtoClass.getSimpleName()).build()).build();

        return new ResponseEntity(body, headers, HttpStatus.OK);
    }

    /**
     * Update/Edit by the HTTP PATCH.
     *
     * @param id          the identifier.
     * @param jsonPayload the json payload.
     * @return the updated/edited data transfer object in standard response of web API.
     */
    @RequestMapping(value = ApiUtil.ID_URL, method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EnvelopeResponse> patch(@PathVariable ID id,
                                                  @RequestBody(required = true) String jsonPayload)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.add(ApiUtil.HTTP_STATUS, String.valueOf(HttpStatus.OK.value()));

        EnvelopeResponse<T> body = new EnvelopeResponse.Builder<T>()
            .data(genericService.update(id, jsonPayload))
            .meta(new Meta.Builder().type(dtoClass.getSimpleName()).build()).build();

        return new ResponseEntity(body, headers, HttpStatus.OK);
    }
}
