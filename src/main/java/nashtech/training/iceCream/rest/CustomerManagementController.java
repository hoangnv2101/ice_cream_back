package nashtech.training.iceCream.rest;


import nashtech.training.iceCream.dto.CustomerDto;
import nashtech.training.iceCream.service.CustomerManagementService;
import nashtech.training.iceCream.service.GenericService;
import nashtech.training.iceCream.util.ApiUtil;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = ApiUtil.CUSTOMER_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerManagementController  extends GenericController<CustomerDto, Integer>{

    private CustomerManagementService customerManagementService;
    /**
     * Constructor.
     *
     * @param customerManagementService {@link GenericService}.
     */
    public CustomerManagementController(CustomerManagementService customerManagementService) {
        super(customerManagementService);
        this.customerManagementService = customerManagementService;
    }
}
