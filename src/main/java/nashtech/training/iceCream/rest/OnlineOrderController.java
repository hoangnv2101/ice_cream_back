package nashtech.training.iceCream.rest;

import nashtech.training.iceCream.dto.OnlineOrderDto;
import nashtech.training.iceCream.service.GenericService;
import nashtech.training.iceCream.service.OnineOrderService;
import nashtech.training.iceCream.util.ApiUtil;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = ApiUtil.ONLINE_ORDER_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class OnlineOrderController  extends GenericController<OnlineOrderDto, Integer>{

    private OnineOrderService onineOrderService;

    /**
     * Constructor.
     *
     * @param onineOrderService {@link GenericService}.
     */
    public OnlineOrderController(OnineOrderService onineOrderService) {
        super(onineOrderService);
        this.onineOrderService = onineOrderService;
    }
}
