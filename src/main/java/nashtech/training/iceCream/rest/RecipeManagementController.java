package nashtech.training.iceCream.rest;

import nashtech.training.iceCream.dto.Page;
import nashtech.training.iceCream.dto.RecipeDto;
import nashtech.training.iceCream.service.GenericService;
import nashtech.training.iceCream.service.RecipeManagementService;
import nashtech.training.iceCream.util.ApiUtil;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = ApiUtil.RECIPE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class RecipeManagementController extends GenericController<RecipeDto, Integer>{

    private RecipeManagementService recipeManagementService;

    /**
     * Constructor.
     *
     * @param recipeManagementService {@link GenericService}.
     */
    public RecipeManagementController(RecipeManagementService recipeManagementService) {
        super(recipeManagementService);
        this.recipeManagementService = recipeManagementService;
    }

    @GetMapping(value=ApiUtil.RECIPE_TOP_VIEW_URL, params = { "maxResult"})
    public Page<RecipeDto> getRecipeOrderByViewNumberDesc(@RequestParam(name = "maxResult") int maxResult)
    {
        return this.recipeManagementService.getRecipeOrderByViewNumberDesc(maxResult);
    }

    @GetMapping(value=ApiUtil.RECIPE__TOP_NEW_URL,params = {"maxResult"})
    public Page<RecipeDto> getRecipesNewest(@RequestParam(name = "maxResult") int maxResult)
    {
        return this.recipeManagementService.getRecipesNewest(maxResult);
    }
}
